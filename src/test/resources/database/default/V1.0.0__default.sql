CREATE TABLE mailing_lists (
  list_name varchar(255) NOT NULL DEFAULT '',
  list_description varchar(255) DEFAULT NULL,
  is_private bit DEFAULT 1,
  project_id varchar(100) NOT NULL,
  list_short_description varchar(255) DEFAULT NULL,
  is_for_news_archives bit NOT NULL DEFAULT 0,
  create_archives bit NOT NULL DEFAULT 1,
  is_disabled bit NOT NULL DEFAULT 0,
  is_deleted bit NOT NULL DEFAULT 0,
  is_subscribable bit NOT NULL DEFAULT 1,
  create_date TIMESTAMP(6) DEFAULT NULL,
  created_by varchar(20) DEFAULT NULL,
  provision_status varchar(255) DEFAULT NULL
);

INSERT INTO mailing_lists(list_name, list_description, is_private, project_id, list_short_description, is_for_news_archives, create_archives,is_disabled,is_deleted,is_subscribable,create_date,created_by)
  VALUES('ee4j-dev', 'description', 0, 'ee4j', 'short description', 0, 1, 0, 0,1,CURRENT_TIMESTAMP(),'SYSTEM');
INSERT INTO mailing_lists(list_name, list_description, is_private, project_id, list_short_description, is_for_news_archives, create_archives,is_disabled,is_deleted,is_subscribable,create_date,created_by)
  VALUES('eclipse-dev', 'description', 0, 'eclipse', 'short description', 0, 1, 0, 0,1,CURRENT_TIMESTAMP(),'SYSTEM');
INSERT INTO mailing_lists(list_name, list_description, is_private, project_id, list_short_description, is_for_news_archives, create_archives,is_disabled,is_deleted,is_subscribable,create_date,created_by)
  VALUES('internal-dev', 'description', 1, 'technology.dash', 'short description', 0, 1, 0, 0, 1, CURRENT_TIMESTAMP(),'SYSTEM');
  
CREATE TABLE mailing_list_subscriptions (
  email varchar(254) NOT NULL DEFAULT '',
  list_name varchar(254) NOT NULL DEFAULT '',
  digest bit DEFAULT 0,
  subscribed TIMESTAMP(6) NOT NULL
);
INSERT INTO mailing_list_subscriptions(email, list_name, digest, subscribed)
  VALUES('sample@eclipse.org','ee4j-dev', 0, CURRENT_TIMESTAMP());
INSERT INTO mailing_list_subscriptions(email, list_name, digest, subscribed)
  VALUES('sample2@eclipse.org','ee4j-dev', 0, CURRENT_TIMESTAMP());
INSERT INTO mailing_list_subscriptions(email, list_name, digest, subscribed)
  VALUES('sample3@eclipse.org','ee4j-dev', 0, CURRENT_TIMESTAMP());
INSERT INTO mailing_list_subscriptions(email, list_name, digest, subscribed)
  VALUES('sample@eclipse.org','eclipse-dev', 0, CURRENT_TIMESTAMP());
INSERT INTO mailing_list_subscriptions(email, list_name, digest, subscribed)
  VALUES('sample2@eclipse.org','internal-dev', 0, CURRENT_TIMESTAMP());