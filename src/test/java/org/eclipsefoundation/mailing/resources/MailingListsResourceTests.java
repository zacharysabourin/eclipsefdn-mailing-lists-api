package org.eclipsefoundation.mailing.resources;

import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;

import javax.inject.Inject;

import org.eclipsefoundation.mailing.test.namespace.SchemaNamespaceHelper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.quarkus.test.junit.QuarkusTest;

@TestInstance(Lifecycle.PER_CLASS)
@QuarkusTest
class MailingListsResourceTests {
    public static final String MAILING_LISTS_BASE_URL = "/mailing-list";
    public static final String AVAILABLE_MAILING_LISTS_URL = MAILING_LISTS_BASE_URL + "/available";
    public static final String MAILING_LISTS_BY_NAME_URL = MAILING_LISTS_BASE_URL + "/{listName}";

    public static final String PROJECTS_MAILING_LISTS_URL = MAILING_LISTS_BASE_URL + "/projects";
    public static final String AVAILABLE_PROJECTS_MAILING_LISTS_URL = PROJECTS_MAILING_LISTS_URL + "/available";
    public static final String PROJECT_MAILING_LISTS_URL = PROJECTS_MAILING_LISTS_URL + "/{projectID}";

    @Inject
    ObjectMapper json;

    @Test
    void getMailingLists_success() {
        given().when().get(MAILING_LISTS_BASE_URL).then().statusCode(200);
    }

    @Test
    void getMailingLists_success_format() {
        given().when().get(MAILING_LISTS_BASE_URL).then().assertThat()
                .body(matchesJsonSchemaInClasspath(SchemaNamespaceHelper.MAILING_LISTS_SCHEMA_PATH));
    }

    @Test
    void getAvailableMailingLists_success() {
        given().when().get(AVAILABLE_MAILING_LISTS_URL).then().statusCode(200);
    }

    @Test
    void getAvailableMailingLists_success_format() {
        given().when().get(AVAILABLE_MAILING_LISTS_URL).then().assertThat()
                .body(matchesJsonSchemaInClasspath(SchemaNamespaceHelper.MAILING_LISTS_SCHEMA_PATH));
    }

    @Test
    void getMailingList_success() {
        given().when().get(MAILING_LISTS_BY_NAME_URL, "eclipse-dev").then().statusCode(200);
    }

    @Test
    void getMailingList_success_format() {
        given().when().get(MAILING_LISTS_BY_NAME_URL, "eclipse-dev").then().assertThat()
                .body(matchesJsonSchemaInClasspath(SchemaNamespaceHelper.MAILING_LIST_SCHEMA_PATH));
    }

    @Test
    void getProjectsMailingLists_success() {
        given().when().get(PROJECTS_MAILING_LISTS_URL).then().statusCode(200);
    }

    @Test
    void getProjectsMailingLists_success_format() {
        given().when().get(PROJECTS_MAILING_LISTS_URL).then().assertThat()
                .body(matchesJsonSchemaInClasspath(SchemaNamespaceHelper.MAILING_LIST_MAPPING_SCHEMA_PATH));
    }

    @Test
    void getAvailableProjectsMailingLists_success() {
        given().when().get(AVAILABLE_PROJECTS_MAILING_LISTS_URL).then().statusCode(200);
    }

    @Test
    void getAvailableProjectsMailingLists_success_format() {
        given().when().get(AVAILABLE_PROJECTS_MAILING_LISTS_URL).then().assertThat()
                .body(matchesJsonSchemaInClasspath(SchemaNamespaceHelper.MAILING_LIST_MAPPING_SCHEMA_PATH));
    }

    @Test
    void getProjectMailingLists_success() {
        given().when().get(PROJECT_MAILING_LISTS_URL, "ee4j").then().statusCode(200);
    }

    @Test
    void getProjectMailingLists_success_format() {
        given().when().get(PROJECT_MAILING_LISTS_URL, "ee4j").then().assertThat()
                .body(matchesJsonSchemaInClasspath(SchemaNamespaceHelper.MAILING_LISTS_SCHEMA_PATH));
    }
}
