clean:;
	mvn clean
compile-java: generate-spec;
	mvn compile package
compile-java-quick: generate-spec;
	mvn compile package -Dmaven.test.skip=true
compile: clean compile-java;
compile-quick: clean compile-java-quick;
install-yarn:;
	yarn install --frozen-lockfile --audit
generate-spec: install-yarn validate-spec;
	yarn run generate-json-schema
validate-spec: install-yarn;
compile-start: compile-quick;
	docker-compose down
	docker-compose build
	docker-compose up
start-spec: validate-spec;
	yarn run start