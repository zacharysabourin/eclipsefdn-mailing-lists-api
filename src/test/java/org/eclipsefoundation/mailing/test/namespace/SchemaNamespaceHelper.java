package org.eclipsefoundation.mailing.test.namespace;

public final class SchemaNamespaceHelper {
    public static final String BASE_SCHEMAS_PATH = "schemas/";
    public static final String BASE_SCHEMAS_PATH_SUFFIX = "-schema.json";
    public static final String MAILING_LISTS_SCHEMA_PATH = BASE_SCHEMAS_PATH + "mailing-lists"
            + BASE_SCHEMAS_PATH_SUFFIX;
    public static final String MAILING_LIST_SCHEMA_PATH = BASE_SCHEMAS_PATH + "mailing-list" + BASE_SCHEMAS_PATH_SUFFIX;
    public static final String MAILING_LIST_MAPPING_SCHEMA_PATH = BASE_SCHEMAS_PATH + "mailing-list-mapping"
            + BASE_SCHEMAS_PATH_SUFFIX;
}
